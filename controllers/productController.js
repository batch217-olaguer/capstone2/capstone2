const auth = require("../auth.js")
const bcrypt = require("bcrypt")
const Product = require("../models/Product.js")
const User = require("../models/User.js")
const productRoute = require("../routes/productRoutes.js")


// Get all products
module.exports.getAllProducts = (data) => {
    if (data.isAdmin || data.isSuperAdmin) {
        return Product.find({}).then(result => {
            return result
        }).catch((error) => {
            return message `Access denied.`
        })
    }
}

// Basic User features
// Get active products
module.exports.getActiveProducts = () => {
    return Product.find({isActive: true}).then(result => {
        return result
    })
}

// Get single product
module.exports.getProduct = (productId) => {
    return Product.findById(productId).then(result => {
        return result
    })
}

// Admin and superAdmin features
// List a product 
module.exports.listProduct = (data) => {
    if (data.isAdmin || data.isSuperAdmin){
        let newProduct = new Product ({
            name: data.product.name,
            tag: data.product.tag,
            description: data.product.description,
            price: data.product.price,
            stocks: data.product.stocks
        })
        return newProduct.save().then((newProduct, error) => {
            if (error) {
                return `${data.product.name} not added. Something went wrong.`
            }
            return `Product ${data.product.name} has been successfully listed.`
        })
    }
    let message = Promise.resolve("Feature not available.")
    return message.then((value) => {
        return value
    }).catch((error) => {
            console.log(error)
        return {
            message: `User does not exist.`
        }
    })
}

// Get all products [active and not active]
/*module.exports.getProducts = () => {
    return Product.find({isActive: true}).then(result => {
        return result
    })
}*/
/*module.exports.getAllListedProducts = (data) => {
    return Product.find({}).then((result) => {
        return result
    }).catch((error) => {
        console.log(error)
        return{
            message: `Something went wrong.`
        }
    })
}*/
/*module.exports.getAllProducts = (data) => {
    if (data.isAdmin || data.isSuperAdmin) {
        return Product.find({}).then(result => {
            return result
        }).catch((error) => {
            console.log(error)
            return {
                message: `Something went wrong.`
            }
        })
    }return Promise.resolve({
        message: `Access to feature is denied.`
    })
}*/

// Update product
module.exports.updateProduct = (productId, data) => {
    console.log(data.isAdmin)
    console.log(data.isSuperAdmin)
    if (data.isAdmin || data.isSuperAdmin) {
        return Product.findByIdAndUpdate(productId, {
            name: data.product.name,
            tag: data.product.tag,
            description: data.product.description,
            price: data.product.price,
            stocks: data.product.stocks
        }).then((updatedProd, error) => {
            console.log(updatedProd)
            if(error) {
                return false
            }
            return `Product has been successfully updated.`
        })
    }
    let message = Promise.resolve("User must be Admin to access this.")
    return message.then((value) => {
        return value
    })
}

/*module.exports.updateProduct = (productId, data) => {
    if (data.isAdmin || data.isSuperAdmin) {
        return Product.findByIdAndUpdate(productId, {
            name: data.name,
            description: data.description,
            price: data.price
        }).then((updatedProduct, error) => {
            if (error) {
                return `Something went wrong. ${newData.name} update failed.`
            }
            return `${newData.name} has been successfully updated.`
        })
    }
}*/

// Archive products
module.exports.archiveProduct = (productId, data) => {
    if (data.isAdmin || data.isSuperAdmin) {
        return Product.findByIdAndUpdate(productId, {
            isActive: false
        }).then((archivedProduct, error) => {
            if (error) {
                return `Product cannot be found.`
            }
            return `Product successfully archived.`
        })
    }
    return Promise.resolve({
        message: `Access to feature is denied.`
    })
}

// Unarchive products
module.exports.unarchiveProduct = (productId, data) => {
    if (data.isAdmin || data.isSuperAdmin) {
        return Product.findByIdAndUpdate(productId, {
            isActive: true
        }).then((archivedProduct, error) => {
            if (error) {
                return `Product cannot be found.`
            }
            return `Product successfully removed from archives.`
        })
    }
    return Promise.resolve({
        message: `Access to feature is denied.`
    })
}

