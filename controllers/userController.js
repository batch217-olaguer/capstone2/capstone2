const auth = require("../auth.js");
const bcrypt = require("bcrypt");
const Product = require("../models/Product.js");
const User = require("../models/User.js");
const Order = require("../models/Order.js");

// Get all users [active and not active]
module.exports.getAllUsers = (data) => {
    if (data.isAdmin || data.isSuperAdmin) {
        return User.find({}, {
        	cart: 0,
        	password: 0
        }).then(result => {
            return result
        }).catch((error) => {
			return {
				message: `Access denied.`
			}
		})
    } return Promise.resolve({
		message: `Access to feature is denied.`
	})
}

// Check if email exists
module.exports.checkEmailExists = (requestBody) => {
	return User.find({email: requestBody.email}).then(result => {
		if(result.length > 0) {
			return `${requestBody.email} is already registered.`;
		}
		return `${requestBody.email} is available for registration.`;
	})
}

// User registration
module.exports.registerUser = (requestBody) => {
	return User.find({email: requestBody.email}).then(result => {
		if(result.length > 0){
			return(`${requestBody.email} already exists.`)
		} else {
			return User.find({username: requestBody.username}).then(result => {
				if (result.length > 0){
					return(`${requestBody.username} is already taken.`)
				} else {
					return User.find({mobileNo: requestBody.mobileNo}).then(result => {
						if (result.length > 0){
							return(`${requestBody.mobileNo} is already taken.`)
						} else {
							let newUser = new User ({
								firstName: requestBody.firstName,
								lastName: requestBody.lastName,
								age: requestBody.age,
								username: requestBody.username,
								email: requestBody.email,
								password: bcrypt.hashSync(requestBody.password, 10),
								mobileNo: requestBody.mobileNo
							})
						return newUser.save().then((user, error) => {
							if(error) {
								return "User registration failed!"
							}
							return `User ${requestBody.username} has successfully been created!`
							})
						}
					})
				}
			})
		} 
	}).catch((error) => {
		console.log(error)
		return {
			message: `Mobile number is already registered.`
		}
	})	
} 

// login using email.
module.exports.loginUser = (requestBody) => {
	return User.findOne({$or : [{username: requestBody.username}, {email: requestBody.email}, {mobileNo: requestBody.mobileNo}], }).then(result => {
		if (result == null){
			return `Please input your username, email or mobile number.`
		} else {
			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccesstoken(result)}
			}
			return "Login failed. Username or email or password is incorrect."
		}
	})
}

// Get single user from token
module.exports.getProfile = (requestBody) => {
	return User.findById(requestBody.userId, {
		password:0,
		cart: 0,
		orders: 0
	}).then(result => {
		return result;
	}) 
}

// Admin and SuperAdmin features
// Set user as Admin
module.exports.setAsAdmin = (data) => {
    if (data.isSuperAdmin){
        return User.findById(data.user.userId).then((userAdResult) =>{
            if(userAdResult.isAdmin){
                return {
                    message: `User is already an Admin.`
                }
            }

            return User.findByIdAndUpdate(data.user.userId, {
                isAdmin: true
            }).then((adminUser,error) => {
                if(error){
                    return{
                    message: `Something went wrong`
                    }    
                }
                return {
                    message: `User restriction level set to: Admin.`
                }
            })
        }).catch((error) => {
        	console.log(error)
            return {
                message: `User does not exist`
            }
        })   
    }

    return Promise.resolve({
        message: `Access denied.`
    })
}


// Add to Cart
module.exports.addToCart = async (data) => {
    if (data.isAdmin) {
    	return "the Cart feature is for customers only."
    } else {
    	const productID = data.productInfo.productId
        console.log(productID)
        const productInfo = await Product.findById(productID)
        console.log(productInfo)
        let userCart = await User.findById(data.userId)
        .then(user => {
            user.cart.unshift({
                productId: data.productInfo.productId,
                name: productInfo.name,
                tag: productInfo.tag,
                description: productInfo.description,
                price: productInfo.price,
                quantity: data.productInfo.quantity,
                subtotal: productInfo.price*data.productInfo.quantity //price*quantity
            })
            //console.log(user.cart[1].subtotal)
            return user.save().then((user, error) => {
                if (error) {
                    console.log(error)
                    return "Something went wrong."
                }
                return true
            })
        })
        // 

	    const myCart = await User.findById(data.userId)
	    // console.log(myCart)
	    const oldTotal = myCart.cartTotal

	    if(userCart){
	    	User.findByIdAndUpdate((data.userId), {
	    		cartSize: myCart.cart.length,
	    		cartTotal: oldTotal + myCart.cart[0].subtotal
	    	}).then((updatedTotal, error) => {
	    		if (error) {
	    			return {message: "Something went wrong."}
	    		}
	    		return {message: "Cart updated successfully."}
	    	}).catch((error) => {
			console.log(error)
			return false
		})
	    	return `Successfully added "${data.productInfo.quantity}" pc/s of ${productInfo.name} to cart.`
		}
    }
    return "Don't give up on the error man!"    
}

// with quantity update 
// module.exports.addToCart = async (data) => {
//     if (data.isAdmin) {
//     	return "the Cart feature is for customers only."
//     } else {
//     	var productID = data.productInfo.productId
//         // console.log(productID)
//         var productInfo = await Product.findById(productID)
//         // console.log(productInfo)
//         var userCartItems = await User.findById(data.userId)
//         // console.log(userCartItems)
//         var userProductCart = userCartItems.cart
//         console.log(userProductCart)

//         /*let firstProductQuantity = userProductCart[0].quantity
//         console.log(firstProductQuantity)
//         if (firstProductQuantity == data.productInfo.quantity) {
//         	let checkProductIdQuantity = await User.findByIdAndUpdate(data.userId, {
//         		quantity: firstProductQuantity + data.productInfo.quantity
//         	}).then((checkedProductIdQuantity, error) => {
//         		if (error) {
//         			console.log(error)
//         			return {message: "Something went wrong"}
//         		}
//         		return true
//         	})
//         	console.log(data.productInfo.quantity)
//         }*/
//         let userCart = await User.findById(data.userId)
//         .then(user => {
//             user.cart.unshift({
//                 productId: data.productInfo.productId, // info you put in postman body
//                 name: productInfo.name, // info from productInfo
//                 tag: productInfo.tag, // info from productInfo
//                 description: productInfo.description, // info from productInfo
//                 price: productInfo.price, // info from productInfo
//                 quantity: data.productInfo.quantity, // info you put in postman body
//                 subtotal: productInfo.price*data.productInfo.quantity //price*quantity
//             })
//             //console.log(user.cart[1].subtotal) 
//             return user.save().then((user, error) => {
//                 if (error) {
//                     console.log(error)
//                     return "Something went wrong."
//                 }
//                 // console.log(userProductCart) ok
//                 return true
//             })
//         })
//         // 

// 	    const myCart = await User.findById(data.userId)
// 	    // console.log(myCart)
// 	    const oldTotal = myCart.cartTotal

// 	    if(userCart){
// 	    	User.findByIdAndUpdate((data.userId), {
// 	    		cartSize: myCart.cart.length,
// 	    		cartTotal: oldTotal + myCart.cart[0].subtotal
// 	    	}).then((updatedTotal, error) => {
// 	    		if (error) {
// 	    			return {message: "Something went wrong."}
// 	    		}
// 	    		return {message: "Cart updated successfully."}
// 	    	}).catch((error) => {
// 			console.log(error)
// 			return false
// 			})
// 		}
// 	    	return `Successfully added "${data.productInfo.quantity}" pc/s of ${productInfo.name} to cart.`
//     }
//     return "Don't give up on the error man!"    
// }



// Remove last added from Cart
module.exports.removeFromCart = async (data) => {
    if (data.isAdmin) {
    	return "the Cart feature is for customers only."
    } else {
    	const productID = data.productInfo.productId
        // console.log(productID)
        const productInfo = await Product.findById(productID)
        // console.log(productInfo)
        let userCart = await User.findById(data.userId).then(user => {
            user.cart.shift({
                /*productId: data.productInfo.productId,
                name: productInfo.name,
                tag: productInfo.tag,
                description: productInfo.description,
                price: productInfo.price,
                quantity: data.productInfo.quantity,
                subtotal: productInfo.price*data.productInfo.quantity*/
            })
            //console.log(user.cart[1].subtotal)
            return user.save().then((user, error) => {
                if (error) {
                    console.log(error)
                    return "Something went wrong."
                }
                return true
            })
        })
        // 

	    const myCart = await User.findById(data.userId)
	    const oldTotal = myCart.cartTotal

	    if(userCart){
	    	User.findByIdAndUpdate((data.userId), {
	    		cartSize: myCart.cart.length,
	    		cartTotal: oldTotal + myCart.cart[0].subtotal,    		
	    	}).then((updatedTotal, error) => {
	    		if (error) {
	    			return {message: "Something went wrong."}
	    		}
	    		return {message: "Cart updated successfully."}
	    	}).catch((error) => {
			console.log(error)
			return "Error found bro. You can fix it!" 
		})
	    	return `Successfully removed last item added from cart.`
		}
    }
    return "Don't give up on the error man!"    
}

//Checkout
// CheckOut module WORKING!
module.exports.checkOut = async (data) => {
	if (data.isAdmin) {
		return "The Cart feature is for customers only."
	} else {
		const isCartEmpty = await User.findById(data.userId)
		if (isCartEmpty.cart === null) {
			return "Cart is empty. Nothing to checkout."
		} else {
			// console.log(isCartEmpty.cart[0].name)
			let fullName = `${isCartEmpty.firstName} ${isCartEmpty.lastName}`
			let newOrder = new Order ({
				userId: data.userId ,
				customer_name: fullName,
				orderDetails: [isCartEmpty.cart],
				orderTotal: isCartEmpty.cartTotal,
				// orderStatus: 
			})
			// console.log(orderDetails)
			return newOrder.save().then((order, error) => {
				if(error) {
					return 'Order checkout failed.'
				}
				const myCart = /*await*/ User.findById(data.userId)
				const oldTotal = myCart.cartTotal

				if(isCartEmpty){
					User.findByIdAndUpdate((data.userId), {
					    cartSize: 0,
					    cartTotal: 0,
					    cart: []    		
					}).then((updatedTotal, error) => {
					    if (error) {
					    	return {message: "Something went wrong."}
					    }
					    return {message: "Cart updated successfully."}
					}).catch((error) => {
						console.log(error)
						return "Error found bro. You can fix it!" 
					})
					return `Order placed`
				}
				return 'Something went wrong with your cart.'
				return 'Checkout successful.'
			})

		}
		return 'Failed to create new order.'
	}
}

/* //Checkout Module WIP almost finished
module.exports.checkOut = async (data) => {
	if (data.isAdmin) {
		return "The Cart feature is for customers only."
	} 
	if (data.cart === null) {
		return "Cart is empty. Nothing to checkout."
	} else {
		const isCartEmpty = await User.findById(data.userId)
		let userOrders = isCartEmpty.orders
		// let checkOutOrder = 
		User.findByIdAndUpdate((data), {
			orders: data.cart
		}).then((checkedOut, error) => {
			if(error) {
				return 'Something went wrong. Checkout Failed.'
			}
			return `Checkout Successful.`
		}).catch((error) => {
			console.log(error)
			return false
		})
		return true
			//console.log(userOrder)
			//return `F.`
			// console.log(isCartEmpty.cart[0].name)
			// let fullName = `${isCartEmpty.firstName} ${isCartEmpty.lastName}`
			// let newOrder = new Order ({
			// 	userId: data.userId ,
			// 	customer_name: fullName,
			// 	orderDetails: [isCartEmpty.cart],
			// 	orderTotal: isCartEmpty.cartTotal,
			// 	// orderStatus: 
			// })
			// console.log(orderDetails)
		return 'Failed to create new order.'
	}
	return 'Something went wrong with your cart.'
}*/

/*module.exports.checkOut = async (data) => {
	if(data.isAdmin) {
		return "The Cart feature is for customers only."
	} else {
		let isCartEmpty = await User.findById(data.userId)
		if (isCartEmpty.cart == null) {
			return "Cart is empty. Nothing to checkout."
		}
	}else{
			console.log(isCartEmpty.cart.name)
			let fullName = `${result.firstName} ${result.lastName}`
			let newOrder = new Order ({
				userId: data.userId ,
				customer_name: fullName,
				items: [{
					productId: result.cart[0].productId,
					name: result.cart[1].name,
					tag: result.cart[2].tag,
					price: result.cart[3].price,
					quantity: result.cart[4].quantity,
					subtotal: result.cart[5].subtotal
				}],
				orderTotal: result.cartTotal,
				// orderStatus: 
			})
			return newOrder.save().then((order, error) => {
				if(error) {
					return 'Order checkout failed.'
				}
				return 'Checkout successful.'
			})
		}
		return 'Failed to create new order.'
		})
		return 'Something went wrong with your cart.'
	}
	return false
}*/
/*module.exports.checkOut = async (data) => {
    if (data.isAdmin) {
    	return "the Cart feature is for customers only."
    } else {
    	const productID = data.productInfo.productId
        console.log(productID)
        var userDetails = await User.findById(data.userId)
        let userCartAndProduct = userDetails.cart
        let userOrdersList = userDetails.orders
        let cartCheckOut = userCartAndProduct.map(object => ({object}))
        const productInfo = await Product.findById(productID)
        // console.log(productInfo)
        let userOrdersListCheckout = await User.findById(data.userId)
        .then(user => {
            user.orders.unshift({
                productId: data.productInfo.productId,
                name: userCartAndProduct.name,
                tag: userCartAndProduct.tag,
                description: userCartAndProduct.description,
                price: userCartAndProduct.price,
                quantity: userCartAndProduct.quantity,
                subtotal: productInfo.price*userCartAndProduct.quantity
            })
            //console.log(user.cart[1].subtotal)
            return user.save().then((user, error) => {
                if (error) {
                    console.log(error)
                    return "Something went wrong."
                }
                return true
            })
        })
        // 

	    const myCart = await User.findById(data.userId)
	    // console.log(myCart)
	    const oldTotal = myCart.cartTotal

	    if(userCart){
	    	User.findByIdAndUpdate((data.userId), {
	    		cartSize: myCart.cart.length,
	    		cartTotal: oldTotal + myCart.cart[0].subtotal
	    	}).then((updatedTotal, error) => {
	    		if (error) {
	    			return {message: "Something went wrong."}
	    		}
	    		return {message: "Cart updated successfully."}
	    	}).catch((error) => {
			console.log(error)
			return false
		})
	    	return `Successfully added "${data.productInfo.quantity}" pc/s of ${productInfo.name} to cart.`
		}
    }
    return "Don't give up on the error man!"    
}
*/


/*module.exports.checkOut = async (data) => {
	if (data.isAdmin) {
		return `This feature is for customers only.`
	} else {
		// const productID = data.productInfo.productId
		// const productInfo = await Product.findById(productID)
		var userDetails = await User.findById(data.userId)
		let userCartAndProduct = userDetails.cart
		let userOrdersList = userDetails.orders
		// console.log(productID)
		// console.log(productInfo)
		// console.log(userDetails)
		// console.log(userCartAndProduct)
		// console.log(userOrdersList)
		let cartCheckOut = userCartAndProduct.map(object => ({object}))
		.then(object => {

		})
		// console.log(cartCheckOut)
		let userCart = await User.findById(data.userId).then(user => {
		    // user.order.shift({cartCheckOut})

		})
		// console.log(userCart)
		let newOrder =  new Order

	}
}*/
	



/*
	// finding cart in User.js
	let myCartCheckout = await User.findById(data.userId)
	//console.log(myCartCheckout)

	let arrOrder = await myCartCheckout.orders
	console.log(arrOrder)
	let testPush = await arrOrder.push({
		productId: arrOrder.cart.productId
	})
	return arrOrder.save().then((user, error) => {
	    if (error) {
	 		//console.log(myCartCheckout)
	        //console.log(error)
	        return "Something went wrong."
	    }
	    //console.log(myCartCheckout)
	    return true
	})
	// let arrOrderTotal = myCartCheckout.orders[0].order.orderTotal
	// console.log(arrOrderTotal)
	// let arrCart = myCartCheckout.cart
	// let cartChekout = arrOrder.concat(arrCart)
	// console.log(cartChekout) // transfer cart items to checkout

	
/*	let forCartCheckOut = await User.findByIdAndUpdate((data.userId), {
		arrOrderTotal: myCartCheckout.cartTotal
	}).then((cartCheckedOut, error) => {
		if(error) {
			return {message: "Something went wrong."}
		}
		return "Checkout Successful."
		}).catch((error) => {
			console.log(error)
			return false
		})
		return true


	let clearCart = arrCart.length = 0 // clearing cart.
	// let myCartTotal = Users.findByIdAndUpdate((data.userId), 
*/
/*	let myOrder = myCartCheckout.cart
	for (let i = 0; i > 0; i--) {

	} 
	
}*/

// View Cart
module.exports.viewCart = (requestBody) => {
	return User.findById(requestBody.userId, {
		age:0,
		password:0,
		email:0,
		mobileNo:0,
		isSuperAdmin:0,
		isAdmin:0,
		createdOn:0,
		orders: 0
	}).then(result => {
		return result;
	}) 
}

// View My Orders
module.exports.viewOrders = (requestBody) => {
	return Order.findById(requestBody.userId/*, {
		age:0,
		password:0,
		email:0,
		mobileNo:0,
		isSuperAdmin:0,
		isAdmin:0,
		createdOn:0,
		cart: 0,
		cartTotal: 0
	}*/).then(result => {
		return result;
	})
} 


/*module.exports.viewOrders = (requestBody) => {
	return User.findById(requestBody.userId, {
		age:0,
		password:0,
		email:0,
		mobileNo:0,
		isSuperAdmin:0,
		isAdmin:0,
		createdOn:0,
		cart: 0,
		cartTotal: 0
	}).then(result => {
		return result;
	})
} 
*/