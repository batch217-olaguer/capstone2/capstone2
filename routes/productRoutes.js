// Declaration of Dependencies
const auth = require("../auth.js")
const express = require("express")
const router = express.Router()
const Product = require("../models/Product.js")
const productController = require("../controllers/productController.js")

// Basic User Features
// Get active products
router.get("/", (request, response) => {
    productController.getActiveProducts().then(resultFromController => {
        response.send(resultFromController)
    })
})

// Get all prods
router.get("/all", auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isSuperAdmin: auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productController.getAllProducts(data).then(resultFromController => {
        response.send(resultFromController)
    })
})

// Get single product
router.get("/:productId", (request, response) => {
    productController.getProduct(request.params.productId).then(resultFromController => {
        response.send(resultFromController)
    })
})

// (admin and superAdmin) functions
// Add product 
router.post("/listProduct", auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isSuperAdmin: auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productController.listProduct(data).then(resultFromController => {
        response.send(resultFromController)
    })
})

// Get all products
/*router.get("/a", (request, response) => {
    productController.getProducts().then(resultFromController => {
        response.send(resultFromController)
    })
})*/
/*router.get("/all", auth.verify, (request, response) => {
    const data = {
        product: request.body,
        isSuperAdmin: auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productController.getAllProducts(data).then(resultFromController => {
        response.send(resultFromController)
    })
})*/

// Update product
router.patch("/:productId/update", (request, response) => {
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        isSuperAdmin: auth.decode(request.headers.authorization).isSuperAdmin
    }
    productController.updateProduct(request.params.productId, data).then(resultFromController => {
        response.send(resultFromController)
    })
})

/*router.patch("/:productId/update", auth.verify, (request, response) => {
    let data = {
        product: request.body,
        userId:auth.decode(request.headers.authorization).id,
        isSuperAdmin:auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productController.updateProduct(request.params.productId, request.body).then(resultFromController => {
        response.send(resultFromController)
    })
})*/

// Archive product (soft delete)
router.patch("/:productId/archive", auth.verify, (request, response) => {
    const data = {
        isSuperAdmin:auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productController.archiveProduct(request.params.productId, data).then(resultFromController => {
        response.send(resultFromController)
    })
})

// Archive product (soft delete)
router.patch("/:productId/unarchive", auth.verify, (request, response) => {
    const data = {
        isSuperAdmin:auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productController.unarchiveProduct(request.params.productId, data).then(resultFromController => {
        response.send(resultFromController)
    })
})

module.exports = router
