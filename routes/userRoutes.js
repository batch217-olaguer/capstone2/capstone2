// Declaration of Dependencies
const auth = require("../auth.js")
const express = require("express")
const router = express.Router()
const User = require("../models/User.js")
const userController = require("../controllers/userController.js")
const productController = require("../controllers/productController.js")


// Features

// Get all users [SuperAdmin and Admin only]
router.get("/all", auth.verify, (request, response) => {
    let data = {
        user: request.body,
        isSuperAdmin: auth.decode(request.headers.authorization).isSuperAdmin,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    userController.getAllUsers(data).then(resultFromController => {
        response.send(resultFromController)
    })
})

// Check if email exists
router.post("/checkEmail", (request, response) => {
	userController.checkEmailExists(request.body).then(
		resultFromController => response.send(resultFromController));
})

// Register New User
router.post("/register", (request, response) => {
	userController.registerUser(request.body).then(
		resultFromController => response.send(resultFromController));
})

// Login User
router.post("/login", /*async*/(request, response) => {
	/*const user = users.find(user => user.username = request.body.username)
	if (user == null) {
		return resultFromController.send("Cannot find user.")
	} else {*/
		userController.loginUser(request.body).then(
			resultFromController => response.send(resultFromController));
	// }
})

// Get user details from token
router.post("/details", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization)

	userController.getProfile({userId: userData.id}).then(
		resultFromController => response.send(resultFromController))
})


// Set user as Admin [SuperAdmin only]
router.patch("/setAsAdmin", auth.verify, (request, response) => {
	let data = {
		user: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		isSuperAdmin: auth.decode(request.headers.authorization).isSuperAdmin,
	}
	userController.setAsAdmin(data).then(
		resultFromController => response.send(resultFromController));
})

// Add to Cart [User and SuperAdmin Only]
router.post("/addToCart", auth.verify, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		productInfo: request.body
	}
	userController.addToCart(data).then(resultFromController => response.send(resultFromController))
})

// Remove From Cart [User and SuperAdmin Only]
router.post("/removeFromCart", auth.verify, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin,
		productInfo: request.body
	}
	userController.removeFromCart(data).then(resultFromController => response.send(resultFromController))
})

// Checkout [User and SuperAdmin Only]
router.post("/check-out", auth.verify, (request, response) => {
	let data = {
		userId: auth.decode(request.headers.authorization).id,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	userController.checkOut(data).then(resultFromController => response.send(resultFromController))
})

// Go To User Cart
router.post("/cart", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization)

	userController.viewCart({userId: userData.id}).then(
		resultFromController => response.send(resultFromController))
})

// Get user details from token
router.post("/myOrders", auth.verify, (request, response) => {
	let userData = auth.decode(request.headers.authorization)

	userController.viewOrders({userId: userData.id}).then(
		resultFromController => response.send(resultFromController))
})













module.exports = router