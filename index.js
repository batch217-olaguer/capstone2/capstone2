// Declaration of Dependencies
const bcrypt = require("bcrypt");
const cors = require("cors");
const express = require("express");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const userRoute = require("./routes/userRoutes.js");
const productRoute = require("./routes/productRoutes.js");

const app = express();

// MongoDB section
mongoose.connect("mongodb+srv://admin:admin123@zuitt.pec4mzj.mongodb.net/E-Commerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas."))

// app.use section
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Default Routes section
app.use("/users", userRoute);
app.use("/products", productRoute);

// app.listen section
app.listen(process.env.PORT || 2300, () => {
	console.log(`API is now online at port ${process.env.PORT || 2300}.`);
})
