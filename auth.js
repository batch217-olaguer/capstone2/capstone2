const jwt = require("jsonwebtoken")
const secret = "Capstone2"

module.exports.createAccesstoken = (user) => {
	const data = {
		id: user._id,
		email: user.email,
		username: user.username,
		isSuperAdmin: user.isSuperAdmin,
		isAdmin: user.isAdmin
	}
	return jwt.sign(data, secret, {})
}

// Token verification from user
module.exports.verify = (request,response, next) => {
	let token = request.headers.authorization

	if (typeof token !== "undefined"){
		console.log(token)
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (error, data) => {
			if (error){
				return response.send({
					Server: "Token restriction level: Access Denied."
				})
			} else {
				next()
			}
		})	
	} else {
		return response.send({
			Server: "Authorization failed! No token provided."
		})
	}
}

// To decode the user details from the token.
module.exports.decode = (token) => {
	if(typeof token !== "undefined") {
		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (error, data) => {
			if(error) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}