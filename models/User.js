const mongoose = require("mongoose");
	
	const userSchema = new mongoose.Schema({
		firstName: {
			type: String,
			required: [true, "You must have a first name."]
		},
		lastName: {
			type: String,
			required: [true, "You must have a last name."]
		},
		age: {
			type: Number,
			required: [true, "Please create a username."]
		},
		username: {
			type: String,
			unique: true,
			sparse: true,
			required: [true, "Please create a username."]
		},
		email: {
			type: String,
			unique: true,
			sparse: true,
			required: [true, "Email address is required."]
		},
		password: {
			type: String,
			required: [true, "Must have a password."]
		},
		mobileNo: {
			type: String,
			unique: true,
			required: [true, "Mobile number is required."]
		},
		isSuperAdmin: {
			type: Boolean,
			default : false
		},
		isAdmin: {
			type: Boolean,
			default : false
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		cartSize: {
			type: Number,
			default: 0,
			required: [true, "total"]
		},
		cart: [
			{
				productId: { //0
					type: String,
					required: [true, "Product ID is required."]
				},
				name: { //1
					type: String,
					required: [false, "Product name is required."]
				},
				tag: { //2
					type: String,
					required: [true, "Product tag is required."]
				},
				description: { //3
					type: String,
					required: [true, "Product description."]
				},
				price: { //4
					type: Number,
					required: [true, "Product price is required."]
				},
				quantity: { //5
					type: Number,
					required: [true, "Product quantity is required."]
				},
				subtotal: { //6
					type: Number,
					required: [true, "Subtotal."]
				}
			},
			
		],
		cartTotal: {
			type: Number,
			default: 0,
			required: [true, "total"]
		},

		// orders: [
		// 	{
		// 		/*productId: {
		// 			type: String,
		// 			required: [true, "Product ID is required."]
		// 		},
		// 		name: {
		// 			type: String,
		// 			required: [false, "Product name is required."]
		// 		},
		// 		tag: {
		// 			type: String,
		// 			required: [false, "Product tag is required."]
		// 		},
		// 		description: {
		// 			type: String,
		// 			required: [false, "Product tag is required."]
		// 		},
		// 		price: {
		// 			type: Number,
		// 			required: [false, "Product price is required."]
		// 		},
		// 		quantity: {
		// 			type: Number,
		// 			required: [false, "Product quantity is required."]
		// 		},
		// 		orderTotal: {
		// 			type: Number,
		// 			default: 0,
		// 			required: [true, "total"]
		// 		},
		// 		placedOn: {
		// 			type: Date,
		// 			default: new Date
		// 		},
		// 		orderStatus: {
		// 			type: String,
		// 			default: "To Pay"
		// 		}*/
					
		// 	},
		// ],
		
		orders: [
			{
				order: [
					{
						/*productId: {
							type: String,
							required: [true, "Product ID is required."]
						},
						name: {
							type: String,
							required: [false, "Product name is required."]
						},
						tag: {
							type: String,
							required: [false, "Product tag is required."]
						},
						description: {
							type: String,
							required: [false, "Product tag is required."]
						},
						price: {
							type: Number,
							required: [false, "Product price is required."]
						},
						quantity: {
							type: Number,
							required: [false, "Product quantity is required."]
						},*/
						orderTotal: {
							type: Number,
							default: 0,
							required: [true, "total"]
						},
						placedOn: {
							type: Date,
							default: new Date
						},
						orderStatus: {
							type: String,
							default: "To Pay"
						}
					}
				]
			},
		],
		
	})

module.exports = mongoose.model("User", userSchema);
