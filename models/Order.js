const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "UserId is required."]
	},
	customer_name: {
		type: String,
		required: [true, "Customer name is required."]
	},
	orderDetails: [{

	}],
	/*items: [
				{
					productId: {
						type: String,
						required: [true, "Product ID is required."]
					},
					name: {
						type: String,
						required: [false, "Product name is required."]
					},
					tag: {
						type: String,
						required: [false, "Product tag is required."]
					},
					description: {
						type: String,
						required: [false, "Product tag is required."]
					},
					price: {
						type: Number,
						required: [false, "Product price is required."]
					},
					quantity: {
						type: Number,
						required: [false, "Product quantity is required."]
					},
					subTotal: {
						type: Number,
						default: 0,
						required: [true, "total"]
					}
				}
			],*/
	orderTotal: {
		type: Number,
		required: [true, "Order total is required."]
	},
	placedOn: {
		type: Date,
		default: new Date
	},
	orderStatus: {
		type: String,
		default: "To Pay"
	}
})

module.exports = mongoose.model("Order", orderSchema)