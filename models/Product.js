const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product name is required."]
	},
	tag: {
		type: String,
		required: [true, "Product name is required."]
	},
	description: {
		type: String,
		required: [true, "Product description is required."]
	},
	price: {
		type: Number,
		required: [true, "Product price is required."]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	listedOn: {
		type: Date,
		default: new Date()
	},
})

module.exports = mongoose.model("Product", productSchema)